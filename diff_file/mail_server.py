from email.message import EmailMessage
from functools import cached_property
from os import environ
from smtplib import SMTP, SMTPAuthenticationError, SMTPConnectError, SMTPDataError, SMTPException, SMTPHeloError, \
    SMTPNotSupportedError, SMTPRecipientsRefused, SMTPResponseException, SMTPSenderRefused, SMTPServerDisconnected
from ssl import SSLContext, create_default_context
from typing import Iterable, Optional, Sequence

from loguru import logger

from custom_exceptions import SMTPServerError
from diff_file.constants import MessageAttributes


class MessageGitInfo:
    _keys: tuple[str] = (
        "From",
        "Subject",
        "Content-Type",
        "Content-Transfer-Encoding",
        "Content-Language",
        "MIME-Version",
        "Return-Path")

    def __init__(
            self,
            sender_email: str,
            recipient_email: str,
            message_text: str,
            message_kwargs: MessageAttributes):
        self.sender_email: str = sender_email
        self.recipient_email: str = recipient_email
        self.message_text: str = message_text
        self.message_kwargs: dict[str, str] = message_kwargs.prepare_values()

    def __repr__(self):
        return f"<{self.__class__.__name__}(sender_email={self.sender_email}, recipient_email={self.recipient_email}," \
               f" message_text={self.message_text}, " \
               f"message_kwargs={MessageAttributes.from_message_kwargs(self.message_kwargs)})>"

    def __str__(self):
        return f"Сообщение\n{self.readable}"

    @cached_property
    def _recipient_login(self) -> str:
        return self.recipient_email.split("@")[0]

    def message(self) -> Optional[bytes]:
        if not self.message_text:
            logger.info("Нет сообщений для отправки")
            return
        message: EmailMessage = EmailMessage()
        message.set_payload(self.message_text)
        for k, v in zip(self._keys, tuple(self.message_kwargs.values())):
            message[k] = v
        message["To"] = self.recipient_email
        return message.as_string().encode("utf-8")

    @property
    def readable(self) -> Optional[str]:
        if self.message:
            return self.message().decode("utf-8")
        return


class MailServer:
    """Specifies the mail SMTP server.

    Attributes:
        host (str | None): the SMTP server DNS name, str;\n
        port (int | None): the SMTP server port, int;\n
        user (str | None): the login to authenticate in the server, str;\n
        password (str | None): the password to authenticate in the server, str;\n

    Functions:
        server(): run the SMTP server;\n
        send_email(message: MessageYoutrack): send the message to the mailbox.
    """

    def __init__(
            self,
            host: str = None,
            port: int = None,
            user: str = None,
            password: str = None):
        if host is None:
            host = environ["SMTP_HOST"]
        if port is None:
            port = environ["SMTP_PORT"]
        if user is None:
            user = environ["SMTP_USER"]
        if password is None:
            password = environ["SMTP_PASSWORD"]
        self.host: str = host
        self.port: int = port
        self.user: str = user
        self.password: str = password
        self.smtp_server: Optional[SMTP] = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(host={self.host}, port={self.port}, user={self.user}, " \
               f"password={self.password})>"

    def __str__(self):
        return f"Mail server: {self.host}:{self.port}, SMTP server active: {self.smtp_server is None}"

    def server(self):
        """Runs the SMTP server to send emails. Further the server is used as a content manager."""
        context: SSLContext = create_default_context()
        smtp_server: SMTP = SMTP(self.host, self.port)
        try:
            logger.info(f"Попытка подключения к SMTP-серверу: {self.host}:{self.port}")
            smtp_server.connect(self.host, self.port)
            # set the connection
            smtp_server.ehlo()
            smtp_server.starttls(context=context)
            smtp_server.ehlo()
            # authenticate
            smtp_server.login(self.user, self.password)
            logger.info(f"Авторизация прошла успешно")
        except (SMTPServerDisconnected | SMTPNotSupportedError | SMTPException | ConnectionRefusedError) as e:
            logger.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise SMTPServerError
        except (SMTPAuthenticationError | SMTPResponseException | SMTPConnectError) as e:
            logger.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise SMTPServerError
        except RuntimeError as e:
            logger.critical(f"{e.__class__.__name__}\n{str(e)}")
            raise SMTPServerError
        else:
            logger.info("SMTP-соединение установлено")
            self.smtp_server = smtp_server
            return

    @staticmethod
    def _send_email(message: MessageGitInfo, smtp_server: SMTP):
        """
        Sends the message to the mailbox.

        :param message: the message to deliver
        :type message: MessageYoutrack
        :param smtp_server: the SMTP server
        :type smtp_server: SMTP
        :return: None.
        """
        if message.message_text is None:
            logger.warning("Назначенных задач нет")
            return
        try:
            smtp_server.sendmail(message.sender_email, message.recipient_email, message.message())
        except (SMTPHeloError | SMTPSenderRefused | SMTPDataError) as e:
            logger.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            return
        except (SMTPNotSupportedError | SMTPRecipientsRefused) as e:
            logger.error(f"{e.__class__.__name__}\n{e.strerror}")
            return
        else:
            logger.info(f"Сообщение отправлено на почтовый ящик {message.recipient_email}")
            return

    def send_emails(self, messages: Iterable[MessageGitInfo] = None):
        """
        Sends the messages to the user_names.

        :param messages: the message to deliver
        :type messages: Sequence[MessageYoutrack]
        :return: None.
        """
        if self.smtp_server is None:
            logger.critical("SMTP-сервер не активен")
            raise SMTPServerError
        if messages is None:
            logger.warning("Нет сообщений для отправки")
            return
        with self.smtp_server as smtp_server:
            for message in messages:
                self._send_email(message, smtp_server)
        # stop the SMTP server
        self.smtp_server.close()
        self.smtp_server = None
        logger.info("SMTP-сервер отключен")
        return
