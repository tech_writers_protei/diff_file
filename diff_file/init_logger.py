import faulthandler
import logging
from functools import cached_property, wraps
from logging import Handler, LogRecord, basicConfig
from pathlib import Path
from sys import stdout as sysout
from types import FrameType
from typing import Mapping, Type, Literal, Any, Callable

from loguru import logger

from constants import LOG_FOLDER

HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


def convert_ns_to_readable(time_ns: int) -> str:
    hours: int
    minutes: int
    seconds: int = time_ns // 10 ** 9
    if seconds == 0:
        return "менее секунды"
    if seconds < 60:
        hours = 0
        minutes = 0
    else:
        hours = 0
        minutes = seconds // 60
        seconds %= 60
        if minutes >= 60:
            hours = minutes // 60
            minutes %= 60
    hour_string: str = f"{hours} ч " if hours != 0 else ""
    minutes_string: str = f"{minutes} мин " if minutes != 0 else ""
    seconds_string: str = f"{seconds} с" if seconds != 0 else ""
    return f"{hour_string}{minutes_string}{seconds_string}"


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    def __init__(
            self,
            file_name: str,
            handlers: Mapping[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers: dict[str, str] = dict()
        self._file_name: str = file_name
        self._handlers: dict[str, str] = {**handlers}

    @staticmethod
    def _no_http(record: dict) -> bool:
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self) -> str:
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self) -> str:
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self) -> str:
        return "{message}"

    def stream_handler(self) -> dict[str, Any]:
        try:
            _logging_level: str | None = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError as e:
            print(f"{e.__class__.__name__}, {str(e)}")
            raise
        else:
            return _handler

    def rotating_file_handler(self) -> dict[str, Any]:
        try:
            _log_path: Path = Path(LOG_FOLDER).joinpath(f"{self._file_name}_debug.log")
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError as e:
            print(f"{e.__class__.__name__}, {str(e)}")
            raise
        else:
            return _handler


def complete_configure_custom_logging(name: str):
    def inner(func: Callable):
        handlers: dict[HandlerType, LoggingLevel] = {
            "stream": "INFO",
            "file_rotating": "DEBUG"
        }

        file_name: str = name

        logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
        stream_handler: dict[str, Any] = logger_configuration.stream_handler()
        rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

        logger.configure(handlers=[stream_handler, rotating_file_handler])

        basicConfig(handlers=[InterceptHandler()], level=0)
        logger.info("================================================================================")
        logger.debug("Запуск программы:")

        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper
    return inner


def logging_func(func: Callable):
    @wraps(func)
    def wrapper(*args, **kwargs):
        _args = ", ".join([str(_arg) for _arg in args])
        _kwargs = ", ".join([f"{k}={v}" for k, v in kwargs.items()])
        logger.debug(f"Функция {func.__name__}\nargs={_args}\nkwargs={_kwargs}")
        result = func(*args, **kwargs)
        logger.debug(f"Результат выполнения: {str(result)}")
        return result
    return wrapper


def logging_faulthandler(func: Callable):
    if not faulthandler.is_enabled():
        faulthandler.enable()

    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    if faulthandler.is_enabled():
        faulthandler.disable()
    return wrapper
