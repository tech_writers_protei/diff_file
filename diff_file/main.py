from typing import Iterable

from loguru import logger

from config import FileJson, JsonContent
from constants import EMAIL, GitFileCommit, JsonFileHandler, MessageAttributes, SMTP_HOST, SMTP_LOGIN, SMTP_PASSWORD, \
    SMTP_PORT, StrNone, UserCommit
from diff_file.mail_server import MailServer, MessageGitInfo
from git_operating import GitGetCommits, GitGetDiff
from init_logger import complete_configure_custom_logging, logging_faulthandler



def processing():
    file_json: FileJson = FileJson("../sources/config.json")
    json_content: JsonContent = file_json.json_content()
    json_content.read()
    json_content.set_files()
    json_file_handler: JsonFileHandler = JsonFileHandler(json_content.timestamp, json_content.json_file_users)
    ids: dict[str, list[GitFileCommit]] = dict()
    for k, v in json_file_handler.projects_users.items():
        logger.info(f"k, v = {k}, {v}")
        git_get_commits: GitGetCommits
        git_get_commits = GitGetCommits(
            k.project_id, path=k.filename, until=json_file_handler.timestamp)
        git_get_commits.get_content()
        logger.info(f"{git_get_commits._content}")
        _id: str = git_get_commits.get_attr(0, "id")
        for _ in v:
            if _ not in ids:
                ids[_] = []
            git_file_commit: GitFileCommit = GitFileCommit(k.project_id, _id)
            ids[_].append(git_file_commit)
    user_commits: list[UserCommit] = [UserCommit(user, commits) for user, commits in ids.items()]
    _responses: dict[str, list[str]] = dict()

    for user_commit in user_commits:
        _responses[user_commit.login] = []
        for commit in user_commit.commits:
            git_get_diff: GitGetDiff = GitGetDiff(commit.project_id, commit.commit_id)
            git_get_diff.get_content()
            _index: int = git_get_diff.find_file("tele2/tree_toc_products.adoc")
            logger.info(f"index = {_index}")
            if _index != -1:
                _diff: StrNone = git_get_diff.get_attr(_index, "diff")
            else:
                logger.error("No file changes found.")
                _diff: StrNone = None
            _responses[user_commit.login].append(_diff)
    return _responses


def get_json_file_handler() -> JsonFileHandler:
    file_json: FileJson = FileJson("../sources/config.json")
    json_content: JsonContent = file_json.json_content()
    json_content.update_timestamp()
    # json_content.read()
    # json_content.set_files()
    # logger.info(f"content = {json_content._content}")
    # logger.info(f"timestamp = {json_content.timestamp}")
    # logger.info(json_content._content.get("timestamp"))
    # logger.info(f"_is_timestamp_none = {json_content._is_timestamp_none}")
    # json_content.set_timestamp()
    # logger.info(f"json_content.timestamp = {json_content.timestamp}")
    # logger.info(f"json_content.json_file_users = {json_content.json_file_users}")
    return JsonFileHandler(json_content.timestamp, json_content.json_file_users)


def get_user_commits(json_file_handler: JsonFileHandler) -> list[UserCommit]:
    logger.info(f"json_file_handler = {json_file_handler}")
    ids: dict[str, list[GitFileCommit]] = dict()
    for k, v in json_file_handler.projects_users.items():
        logger.info(f"k, v = {k}, {v}")
        git_get_commits: GitGetCommits
        git_get_commits = GitGetCommits(k.project_id, path=k.filename, since=json_file_handler.timestamp)
        git_get_commits.get_content()
        logger.info(f"{git_get_commits._content}")
        _id: str = git_get_commits.get_attr(0, "id")
        for _ in v:
            if _ not in ids:
                ids[_] = []
            git_file_commit: GitFileCommit = GitFileCommit(k.project_id, _id)
            ids[_].append(git_file_commit)
    return [UserCommit(user, commits) for user, commits in ids.items()]


def get_responses(user_commits: Iterable[UserCommit]) -> dict[str, list[str]]:
    _responses: dict[str, list[str]] = dict()

    for user_commit in user_commits:
        _responses[user_commit.login] = []
        for commit in user_commit.commits:
            git_get_diff: GitGetDiff = GitGetDiff(commit.project_id, commit.commit_id)
            git_get_diff.get_content()
            _index: int = git_get_diff.find_file("tele2/tree_toc_products.adoc")
            logger.info(f"index = {_index}")
            if _index != -1:
                _diff: StrNone = git_get_diff.get_attr(_index, "diff")
            else:
                logger.error("No file changes found.")
                _diff: StrNone = None
            _responses[user_commit.login].append(_diff)
    return _responses


def get_messages(git_file_commit: GitFileCommit, user_commit: UserCommit):

    from_: str = f"Andrew Tarasov <{EMAIL}>"
    subject_: str = f"Изменение CHANGELOG.txt {git_file_commit.project_id}"
    content_type_: str = "text/plain; charset=UTF-8;"
    content_transfer_encoding_: str = "8bit"
    content_language_: str = "ru"
    mime_version_: str = "1.0"
    return_path_: str = EMAIL
    message_kwargs: MessageAttributes = MessageAttributes(
        from_,
        subject_,
        content_type_,
        content_transfer_encoding_,
        content_language_,
        mime_version_,
        return_path_)

    sender_email: str = EMAIL
    recipient_email: str = f"{user_commit.login}@protei.ru"
    message_text: str = ""
    message_git_info: MessageGitInfo = MessageGitInfo(
        sender_email,
        recipient_email,
        message_text,
        message_kwargs)


def delivery(messages: Iterable[MessageGitInfo]):
    mail_server: MailServer = MailServer(SMTP_HOST, SMTP_PORT, SMTP_LOGIN, SMTP_PASSWORD)
    mail_server.server()
    mail_server.send_emails(messages)




# @logging_faulthandler
# @complete_configure_custom_logging("diff_file")
# def processing():
#     json_content: JsonContent = JsonContent("../sources/config.json")
#     json_content.read()
#     json_content.set_files()
#     json_content.set_timestamp()
#     json_file_handler: JsonFileHandler = JsonFileHandler.from_json_content(json_content)
#
#     ids: dict[str, list[GitFileCommit]] = dict()
#     for k, v in json_file_handler.projects_users.items():
#         git_get_commits: GitGetCommits
#         git_get_commits = GitGetCommits(k.project_id, path=k.filename, since=json_file_handler.timestamp)
#         git_get_commits.get_content()
#         _id: str = git_get_commits.get_attr(0, "id")
#         for _ in v:
#             if _ not in ids:
#                 ids[_] = []
#             git_file_commit: GitFileCommit = GitFileCommit(k.project_id, _id)
#             ids[_].append(git_file_commit)
#
#     user_commits: list[UserCommit] = [UserCommit(user, commits) for user, commits in ids.items()]
#     _messages: dict[str, list[str]] = dict()
#
#     for user_commit in user_commits:
#         _messages[user_commit.login] = []
#         for commit in user_commit.commits:
#             git_get_diff: GitGetDiff = GitGetDiff(commit.project_id, commit.commit_id)
#             git_get_diff.get_content()
#             _index: int = git_get_diff.find_file("tele2/tree_toc_products.adoc")
#             logger.info(f"index = {_index}")
#             if _index != -1:
#                 _diff: StrNone = git_get_diff.get_attr(_index, "diff")
#             else:
#                 logger.error("No file changes found.")
#                 _diff: StrNone = None
#             _messages[user_commit.login].append(_diff)
#     return _messages





@logging_faulthandler
@complete_configure_custom_logging("diff_file")
def run_script():
    # project_id: str = "tech_writers_protei%2Fterms"
    # ref_name: str = "master"
    #
    # git_get_commits = GitGetCommits(project_id, ref_name=ref_name)
    # git_get_commits.get_content()
    # logger.info(git_get_commits.get_last_commit())
    # logger.info(type(git_get_commits.get_last_commit()))
    # branch_or_sha: str = git_get_commits.get_attr(0, "id")
    #
    # git_get_diff: GitGetDiff = GitGetDiff(project_id, branch_or_sha)
    # git_get_diff.get_content()
    # print(git_get_diff[0].get("diff"))
    json_content: JsonContent = JsonContent("../sources/config.json")
    json_content.read()
    json_content.set_files()
    # logger.info(json_content._content.get("files"))
    # logger.info(json_content.iso_time)
    # logger.info(json_content.logins)
    # logger.info(json_content.json_login_projects[0].json_file_projects)
    # logger.info(json_content.json_file_users)
    for _ in json_content.json_file_users:
        get_commits: GitGetCommits = GitGetCommits(_.project_id)
    # json_content.set_timestamp()


# @logging_faulthandler
# @complete_configure_custom_logging("diff_file")
# def main():
#     _json_file_handler: JsonFileHandler = process_json_file()
#     logger.info(f"_json_file_handler = {_json_file_handler}")
#     user_commits: list[UserCommit] = send_git_requests(_json_file_handler)
#     logger.info(f"user_commits = {user_commits}")
#     for user_commit in user_commits:
#         for git_file_commit in user_commit.commits:
#             logger.info(f"git_file_commit = {get_commit_content(git_file_commit)}")


if __name__ == '__main__':
    # _json_file_handler: JsonFileHandler = get_json_file_handler()
    # logger.info(f"_json_file_handler = {_json_file_handler}")
    # _user_commits: list[UserCommit] = get_user_commits(_json_file_handler)
    # logger.info(f"_user_commits = {_user_commits}")
    # _responses = get_responses(_user_commits)
    # logger.info(_responses)
    _ = processing()
    logger.info(_)
