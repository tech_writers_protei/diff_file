from base64 import b64decode
from http.client import HTTPResponse
from json import loads
from os import environ
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, NamedTuple
from urllib.parse import quote_plus
from urllib.request import OpenerDirector, Request, urlopen

from loguru import logger

from custom_exceptions import RequiredAttributeMissing
from constants import CustomPort, CustomScheme, PATH_TEMP, StrPath

try:
    PROTOCOL: str = environ['PROTOCOL']
    CHUNK_SIZE: int = int(environ['CHUNK_SIZE'])
except KeyError:
    PROTOCOL = "HTTPS"
    CHUNK_SIZE = 4096


class CustomHTTPRequest:
    identifier = 0

    def __init__(
            self, *,
            scheme: str | None = None,
            web_hook: str | None = None,
            host: str | None = None,
            port: int | None = None,
            params: tuple[tuple[str, str], ...] | None = None):
        if scheme is None:
            scheme: str = CustomScheme[f"{PROTOCOL}"].value
        if port is None:
            port: int = CustomPort[PROTOCOL].value
        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: int = port
        self._params: tuple[tuple[str, str], ...] = params
        self.index: int = self.__class__.identifier

        self.__class__.identifier += 1

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self._scheme}, web hook = {self._web_hook}, " \
               f"host = {self._host}, post = {self._port}, params = {self._params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._scheme}, {self._web_hook}, {self._host}, {self._port}," \
               f" {self._params})>"

    def __hash__(self):
        return hash(self.url())

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url() == other.url()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url() != other.url()
        else:
            return NotImplemented

    def _get_params(self) -> str:
        if self._params is None or not self._params:
            return ""
        _params = "&".join([f"{name}={value}" for name, value in self._params])
        # return f"?private_token=glpat-bJDvNZiRMquFuwuDDvq-&{_params}"
        return f"?{_params}"

    def _get_web_hook(self) -> str:
        if self._web_hook is None:
            logger.error("Не указан web hook")
            raise RequiredAttributeMissing
        return self._web_hook.rstrip("/")

    def _get_scheme(self) -> str:
        if self._scheme is None:
            return "https"
        return self._scheme.lower().strip("/")

    def _get_port(self) -> int:
        if self._port is None:
            return CustomPort[PROTOCOL].value
        return self._port

    def _get_host(self) -> str:
        if self._host is None:
            logger.error("Не указан адрес host")
            raise RequiredAttributeMissing
        return self._host.strip("/")

    def url(self) -> str:
        _url: str = f"{self._get_scheme()}://{self._get_host()}/{self._web_hook}{self._get_params()}"
        return quote_plus(_url, ":/&?=%")

    @property
    def host(self) -> str:
        return self._host


class CustomPreparedRequest(NamedTuple):
    url: str
    data: bytes
    headers: dict[str, str]
    host: str
    unverifiable: bool
    method: str
    index: int

    def __str__(self):
        return f"Соединение #{self.index}:\n{str(self.request().full_url)}"

    def __repr__(self):
        return f"<{self.__class__.__name__}: Connection #{self.index} ({self.url}, {self.data}, " \
               f"{[header for header in self.headers]}, " \
               f"{self.host}, {self.unverifiable}, {self.method})>"

    def __hash__(self):
        return hash((self.host, self.headers, self.data))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.request() == other.request()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.request() != other.request()
        else:
            return NotImplemented

    @classmethod
    def from_custom_request(
            cls,
            custom_http_request: CustomHTTPRequest,
            method: str = None,
            data: bytes = None,
            headers: dict[str, str] = None):
        if headers is None:
            headers = dict()
        if data is None:
            data = b''
        return cls(
            custom_http_request.url(),
            data,
            headers,
            custom_http_request.host,
            False,
            method,
            custom_http_request.index)

    def request(self) -> Request:
        if self.headers is None:
            self.headers = dict()
        _headers = {
            "User-Agent": "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11",
        }
        self.headers.update(_headers)
        return Request(self.url, self.data, self.headers, self.host, self.unverifiable, self.method)

    def get_response(self) -> str:
        response: HTTPResponse
        logger.debug(str(self))
        with OpenerDirector().open(self.request().full_url) as response:
            return response.read().decode('utf-8')

    def http_response(self) -> HTTPResponse:
        logger.error(self.request().full_url)
        logger.error(self.request().full_url)
        with OpenerDirector().open(self.request().full_url) as r:
            return r


class CustomHTTPResponse:
    def __init__(self, http_response: HTTPResponse):
        self._http_response: HTTPResponse = http_response

    def __repr__(self):
        return f"<{self.__class__.__name__}(\n{self.response_lines}\n)>"

    def __str__(self):
        return "\n".join(self.response_lines)

    def __hash__(self):
        return hash(self._http_response)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response == other._http_response
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response != other._http_response
        else:
            return NotImplemented

    @property
    def response_lines(self) -> list[str]:
        return [_.decode("utf8") for _ in self._http_response.readlines()]

    def response_json(self):
        return [self._http_response.read()]

    @property
    def response_bytes(self) -> list[bytes]:
        return self._http_response.readlines()

    def __len__(self):
        return len(self.response_bytes)

    def __bool__(self):
        return len(self) > 0

    def __bytes__(self):
        return self._http_response.read()

    def status(self):
        return self._http_response.getcode()


class CustomHTTPResponseChunked(CustomHTTPResponse):
    def __init__(
            self,
            http_response: HTTPResponse, *,
            chunk: int = CHUNK_SIZE):
        super().__init__(http_response)
        self._chunk: int = chunk
        self._response: Any | None = None
        self.get_response()

    def decode_base64(self) -> str:
        return b64decode(self._http_response.read()).decode("utf-8")

    def get_response(self):
        with NamedTemporaryFile(mode="w+b", dir=PATH_TEMP) as fb:
            while chunk := self._http_response.read(self._chunk):
                fb.write(chunk)
            fb.seek(0)
            full_response: list[dict[str, Any]] | dict[str, Any] = loads(fb.read())
            self._response = full_response

    @property
    def response(self):
        return self._response


class HTTPResponseContent:
    def __init__(self, path_file: StrPath):
        if isinstance(path_file, str):
            path_file: Path = Path(path_file)
        self._path_file: Path = path_file.resolve()
        self._content: dict[str, str | bytes] = dict()
        self.read()

    def read(self):
        with open(self._path_file, "rb") as fb:
            _: bytes = fb.read()
        self._content = loads(_)

    def __getitem__(self, item):
        return self._content.get(item)

    def __str__(self):
        return self.__bytes__().decode("utf-8")

    def __bytes__(self):
        return b64decode(self.__getitem__("content"))
