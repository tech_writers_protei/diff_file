from datetime import datetime
from typing import Any
from urllib.request import Request

from loguru import logger
from urllib3 import HTTPResponse

from constants import BoolNone, CommitOrder, DiffMessage, GitLink, HOST, PORT, SCHEME, StrNone, join_path, \
    multiple_replace
from custom_http import CustomHTTPRequest, CustomHTTPResponseChunked, CustomPreparedRequest
from diff_file.custom_exceptions import GitResponseKeyNotFoundError


class GitRequest:
    def __init__(self, git_link: GitLink, project_id: str | int, **kwargs):
        self.git_link: str = str(git_link)
        self.project_id: str = f"{project_id}"
        self._web_hook: StrNone = None
        self._params: Any = None
        self._content: list[dict[str, Any]] | None = None
        for k, v in kwargs.items():
            setattr(self, k, v)

    @property
    def _query_attributes(self) -> tuple[str, ...]:
        raise NotImplementedError

    @property
    def _params_attributes(self) -> tuple[str, ...]:
        raise NotImplementedError

    def _get_web_hook(self):
        _replace_table: dict[str, str] = {
            f"%{attribute}%": f"{getattr(self, attribute)}"
            for attribute in self._query_attributes
        }
        self._web_hook: str = multiple_replace(self.git_link, _replace_table)

    def _get_params(self):
        _values: dict[str, Any] = {
            attribute: getattr(self, attribute)
            for attribute in self._params_attributes
            if getattr(self, attribute) is not None
        }
        _: list[tuple[str, Any]] = [(k, v) for k, v in _values.items()]
        _.append(("private_token", "glpat-bJDvNZiRMquFuwuDDvq-"))
        self._params: tuple[tuple[str, Any]] = *_,

    @property
    def _custom_http_request(self):
        return CustomHTTPRequest(
            scheme=SCHEME.value,
            web_hook=self._web_hook,
            host=HOST,
            port=PORT.value,
            params=self._params)

    def get_content(self):
        self._get_web_hook()
        self._get_params()
        _prepared_request: CustomPreparedRequest
        _prepared_request = CustomPreparedRequest.from_custom_request(self._custom_http_request)
        logger.info(f"_prepared_request = {repr(_prepared_request)}")
        logger.info(f"_prepared_request.url = {_prepared_request.url}")
        # logger.info(f"{_prepared_request.http_response.status}")
        _http_response: HTTPResponse = _prepared_request.http_response()
        logger.info(_http_response.read().decode())
        _chunked: CustomHTTPResponseChunked
        _chunked = CustomHTTPResponseChunked(_http_response)
        self._content = _chunked.response
        logger.info(f"Response: {bool(self)}")

    def write_file(self, name: str):
        _: str = "\n".join([str(_) for _ in self._content])
        with open(join_path(name), "w+") as f:
            f.write(_)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content.__getitem__(item)
        else:
            return NotImplemented

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self._content) != 0

    def __repr__(self):
        return f"<{self.__class__.__name__}({str(self._custom_http_request)})>"

    def __str__(self):
        return f"{self._content}"

    def get_attr(self, item: int, key: str):
        if key not in self._content[item]:
            logger.error(f"Ключ {key} не найден")
            raise GitResponseKeyNotFoundError
        return self._content[item].get(key)


class GitGetPage(GitRequest):
    def __init__(self, project_id: str | int, file_path: str, ref: StrNone):
        git_link: GitLink = GitLink.FILE
        super().__init__(git_link, project_id, file_path=file_path, ref=ref)

    @property
    def _query_attributes(self):
        return "project_id", "file_path"

    @property
    def _params_attributes(self):
        return ("ref",)


def print_bool(boolean: bool | None):
    if boolean is None:
        return
    _conversion: dict[bool, str] = { True: "true", False: "false" }
    return _conversion[boolean]

class GitGetCommits(GitRequest):
    def __init__(
            self,
            project_id: str | int, *,
            ref_name: StrNone = None,
            since: datetime | None = None,
            until: datetime | None = None,
            path: StrNone = None,
            author: StrNone = None,
            with_stats: BoolNone = None,
            first_parent: BoolNone = None,
            order: CommitOrder | None = None,
            trailers: BoolNone = None
    ):
        git_link: GitLink = GitLink.COMMIT
        _since: str = f'{since}Z' if since is not None else None
        _until: str = f'{until}Z' if until is not None else None
        super().__init__(
            git_link,
            project_id,
            ref_name=ref_name,
            since=_since,
            until=_until,
            path=path,
            author=author,
            with_stats=print_bool(with_stats),
            first_parent=print_bool(first_parent),
            order=order,
            trailers=trailers)

    @property
    def _query_attributes(self):
        return ("project_id",)

    @property
    def _params_attributes(self):
        return (
            "ref_name", "since", "until", "path", "author", "with_stats", "first_parent",
            "order", "trailers")

    def get_last_commit(self) -> dict[str, Any]:
        return self._content[0]


class GitGetDiff(GitRequest):
    def __init__(self, project_id: str | int, branch_or_sha: str):
        git_link: GitLink = GitLink.DIFF
        super().__init__(git_link, project_id, branch_or_sha=branch_or_sha)

    @property
    def _query_attributes(self):
        return "project_id", "branch_or_sha"

    @property
    def _params_attributes(self):
        return ()

    def find_file(self, file_name: str):
        for index, item in enumerate(self._content):
            if item.get("new_path") == file_name:
                return index
        else:
            return -1

    def diff_messages(self):
        self.get_content()
        return [
            DiffMessage(
                item.get("diff"),
                item.get("new_path"),
                item.get("old_path"),
                item.get("new_file"),
                item.get("renamed_file"),
                item.get("deleted_file"))
            for item in self._content
        ]
