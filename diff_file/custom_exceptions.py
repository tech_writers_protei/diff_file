from smtplib import SMTPException


class RequiredAttributeMissing(Exception):
    """Required attribute has no specified value."""


class MatchPatternError(ValueError):
    """Datetime does not match the pattern."""


class GitResponseKeyNotFoundError(KeyError):
    """Key is not found in the GitResponse item."""


class FileJsonKeyError(KeyError):
    """Key is not found in the JSON file."""


class FileJsonKeyTypeError(TypeError):
    """Key has an invalid type."""


class JsonContentKeyError(KeyError):
    """Invalid JsonContent key type or value."""


class JsonContentTimestampValueError(ValueError):
    """Invalid value to set to the timestamp."""


class SMTPServerError(SMTPException):
    """SMTP server has failed."""
