import os.path as p
from enum import Enum
from os import environ
from pathlib import Path
from re import Pattern, compile
from typing import Mapping, NamedTuple, TypeAlias


StrPath: TypeAlias = str | Path
StrNone: TypeAlias = str | None
IntNone: TypeAlias = int | None
BoolNone: TypeAlias = bool | None


class CustomPort(Enum):
    HTTP = 80
    HTTPS = 443

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class CustomScheme(Enum):
    HTTP = "http"
    HTTPS = "https"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class GitLink(Enum):
    FILE: str = "api/v4/projects/%project_id%/repository/files/%file_path%"
    COMMIT: str = "api/v4/projects/%project_id%/repository/commits"
    DIFF: str = "api/v4/projects/%project_id%/repository/commits/%branch_or_sha%/diff"

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"


class CommitOrder(Enum):
    DEFAULT = "default"
    TOPO = "topo"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"

    def __str__(self):
        return f"{self.__class__.__name__}[{self._value_}]"


class ConvertPattern(NamedTuple):
    pattern: Pattern
    match_date: tuple[int, int, int]
    match_time: tuple[int, int, int, int]

    @property
    def match_groups(self):
        return [*self.match_date, *self.match_time]

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.pattern.pattern}, {self.match_groups})>"

    def __str__(self):
        _groups: str = ", ".join([str(_) for _ in self.match_groups])
        return f"{self.pattern.pattern}, groups: {_groups}"


def join_path(file_name: str):
    return p.join(parent, file_name)


def multiple_replace(line: str, replace_table: Mapping[str, str]):
    for k, v in replace_table.items():
        line = line.replace(k, v)
    return line


def get_mail(login: str):
    return f"{login}@protei.ru"


class GitFileCommit(NamedTuple):
    project_id: str
    commit_id: str

    def __str__(self):
        return f"{self.project_id}, {self.commit_id}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.project_id}, {self.commit_id})>"

    def __hash__(self):
        return hash((self.project_id, self.commit_id))

    def __key(self):
        return self.project_id, self.commit_id

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented


class UserCommit(NamedTuple):
    login: str
    commits: list[GitFileCommit] = []

    def __str__(self):
        return f"{self.login}, {self.commits}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.login}, {self.commits})>"

    def __iter__(self):
        return iter(self.commits)

    def __len__(self):
        return len(self.commits)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.commits
        else:
            return False

    def __bool__(self):
        return len(self) != 0

    def __hash__(self):
        return hash(self.login)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.login == other.login
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.login != other.login
        else:
            return NotImplemented

    @property
    def projects(self) -> list[str]:
        return [_.project_id for _ in self.commits]


class JsonFileProject(NamedTuple):
    """Gets <user> key from the JSON file.

    Attributes:
        project_id (str): the project_id key;
        branch (str | None): the branch key;
        filename (str | None): the filename key;
    """
    project_id: str
    branch: StrNone
    filename: StrNone

    def __str__(self):
        _branch: str = "" if self.branch is None else f", {self.branch}"
        _filename: str = "" if self.filename is None else f", {self.filename}"
        return f"{self.project_id}{_branch}{_filename}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.project_id}, {self.branch}, {self.filename})>"

    def __key(self):
        return self.project_id, self.filename

    def __hash__(self):
        return hash((self.project_id, self.filename))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented


class JsonFileUser(NamedTuple):
    """Gets 'files' key from the JSON file.

    Attributes:
        login (str): the login key;
        project_id (str): the project_id key;
        branch (str | None): the branch key;
        filename (str | None): the filename key;
    """
    login: str
    project_id: str
    branch: StrNone
    filename: StrNone

    def __str__(self):
        _branch: str = "" if self.branch is None else f", {self.branch}"
        _filename: str = "" if self.filename is None else f", {self.filename}"
        return f"{self.login}, {self.project_id}{_branch}{_filename}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.login}, {self.project_id}, " \
               f"{self.branch}, {self.filename})>"

    def __hash__(self):
        return hash((self.login, self.project_id))

    def __key(self):
        return self.login, self.project_id

    @property
    def json_file_project(self) -> JsonFileProject:
        return JsonFileProject(self.project_id, self.branch, self.filename)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented


class JsonLoginProject(NamedTuple):
    """Gets the dictionary of logins and associated project files.

    Attributes:
        login (str): the user login;
        projects (list[dict[str, str]]): the user project files;
    """
    login: str
    projects: list[dict[str, str]]

    @classmethod
    def from_content(cls, files: list[dict[str, list[dict[str, str]]]], login: str):
        projects: list[dict[str, str]] = []
        for file in files:
            if login in file.keys():
                projects: list[dict[str, str]] = file.get(login)
        return cls(login, projects)

    def __iter__(self):
        return iter(self.projects)

    def __len__(self):
        return len(self.projects)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self.projects[item]
        return NotImplemented

    @property
    def json_file_projects(self) -> set[JsonFileProject]:
        return {
            JsonFileProject(_.get("project_id"), _.get("branch"), _.get("filename"))
            for _ in iter(self)}

    def iter_json_file_projects(self):
        return iter(
            {JsonFileProject(_.get("project_id"), _.get("branch"), _.get("filename"))
             for _ in iter(self)})

    @property
    def project_id_values(self):
        return {_.project_id for _ in self.iter_json_file_projects()}

    @property
    def branch_values(self):
        return {_.branch for _ in self.iter_json_file_projects() if _.branch is not None}

    @property
    def filename_values(self):
        return {_.filename for _ in self.iter_json_file_projects() if _.filename is not None}

    def __hash__(self):
        return self.login

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.login == other.login
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.login != other.login
        else:
            return NotImplemented

    @property
    def json_file_users(self) -> list[JsonFileUser]:
        return [
            JsonFileUser(self.login, _.project_id, _.branch, _.filename)
            for _ in self.iter_json_file_projects()]


class JsonFileHandler(NamedTuple):
    timestamp: StrNone
    json_file_users: set[JsonFileUser]

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.timestamp}, {[repr(_) for _ in iter(self)]})>"

    def __str__(self):
        return f"{self.timestamp}\n{self.json_file_users}"

    def __iter__(self):
        return iter(self.json_file_users)

    def __len__(self):
        return len(self.json_file_users)

    @property
    def json_file_projects(self) -> set[JsonFileProject]:
        return {_.json_file_project for _ in self.json_file_users}

    def _get_users(self, json_file_project: JsonFileProject):
        return [_.login for _ in self.json_file_users if _.json_file_project == json_file_project]

    @property
    def projects_users(self) -> dict[JsonFileProject, list[str]]:
        return {
            json_file_project: self._get_users(json_file_project)
            for json_file_project in self.json_file_projects}


class MessageAttributes(NamedTuple):
    """
    Specifies the email message headers:\n
        From, Subject, Content-Type, Content-Transfer-Encoding, Content-Language, MIME-Version, Return-Path;
    """
    from_: str = None
    subject_: str = None
    content_type_: str = None
    content_transfer_encoding_: str = None
    content_language_: str = None
    mime_version_: str = None
    return_path_: str = None

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict()}"

    def prepare_values(self) -> dict[str, str]:
        return {k: v for k, v in self._asdict().items() if v is not None}

    @classmethod
    def from_message_kwargs(cls, message_kwargs: dict[str, str]):
        return cls(
            message_kwargs["from_"],
            message_kwargs["subject_"],
            message_kwargs["content_type_"],
            message_kwargs["content_transfer_encoding_"],
            message_kwargs["content_language_"],
            message_kwargs["mime_version_"],
            message_kwargs["return_path_"])


class DiffMessage(NamedTuple):
    """Message of the GetDiff request.

    Attributes:
        diff (str): the commit file difference;
        new_path (str): the new path to the file;
        old_path (str): the previous path to the file;
        new_file (bool): the flag if the file has been created;
        renamed_file (bool): the flag if the file has been renamed;
        deleted_file (bool): the flag if the file has been removed;
    """

    diff: str
    new_path: str
    old_path: str
    new_file: bool
    renamed_file: bool
    deleted_file: bool

    '''
    'diff': '@@ -28,7 +28,7 @@ |Возможности интерфейсов (API, provisioning) |+ |+ |+++
    |Соответствие спецификациям |+ |+*+ |*
    |Лицензирование |? |? |?
    -|Производительность |- |- |-
    +|Производительность |+ |+ |+++
    |Возможности масштабирования |- |- |-
    |Управление и обслуживание |+ |+ |+++
    |Требования к инфраструктуре |+ |+ |+++
    '''


PATH_TEMP: str = p.dirname(p.dirname(p.abspath(__file__)))
LOG_FOLDER: str = p.join(PATH_TEMP, "_logs")
SCHEME: CustomScheme = CustomScheme.HTTPS
HOST: str = "gitlab.com"
PORT: CustomPort = CustomPort.HTTPS

parent: str = p.dirname(p.expanduser("~"))

YYYY_MM_DD_HH_MM_SS_MSS: Pattern = compile(
    r"(\d{4}).(\d{2}).(\d{2}).(\d{2}).(\d{2}).(\d{2}).(\d{0,3})[+-](\d{2}):(\d{2})")
DD_MM_YYYY_HH_MM_SS_MSS: Pattern = compile(
    r"(\d{2}).(\d{2}).(\d{4}).(\d{2}).(\d{2}).(\d{2}).(\d{0,3})[+-](\d{2}):(\d{2})")

SMTP_HOST: str = "smtp.protei.ru"
SMTP_PORT: int = 587
SMTP_LOGIN: str = "tarasov-a"
SMTP_PASSWORD: str = environ.get("SMTP_PASSWORD")

EMAIL: str = "tarasov-a@protei.ru"
