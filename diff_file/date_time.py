from datetime import date, datetime, time, tzinfo
from re import Match, fullmatch

from loguru import logger

from custom_exceptions import MatchPatternError
from constants import ConvertPattern, DD_MM_YYYY_HH_MM_SS_MSS, StrNone, YYYY_MM_DD_HH_MM_SS_MSS


class DateTime:
    def __init__(self, date_time: StrNone = None):
        if date_time is None:
            date_time: str = datetime.utcnow().isoformat(" ", "milliseconds")
        self._date_time: str = date_time
        self._date: date | None = None
        self._time: time | None = None
        self._timezone: tzinfo | None = None
        self._origin_dt: datetime | None = None
        self._set_values()

    def __str__(self):
        return f"{self.__class__.__name__}: {self._date_time}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._date_time})>"

    @property
    def _pattern(self) -> ConvertPattern:
        if self._date_time[2].isdigit():
            return ConvertPattern(YYYY_MM_DD_HH_MM_SS_MSS, (1, 2, 3), (4, 5, 6, 7))
        else:
            return ConvertPattern(DD_MM_YYYY_HH_MM_SS_MSS, (3, 2, 1), (4, 5, 6, 7))

    @property
    def _match(self):
        return fullmatch(self._pattern.pattern, self._date_time)

    def _validate(self):
        if not self._match:
            logger.error(f"Дата и время {self._date_time} не подходит под "
                         f"маску {self._pattern.pattern}")
            raise MatchPatternError
        return self._match

    def _set_values(self):
        try:
            _m: Match = self._validate()
        except MatchPatternError:
            self._date_time = datetime.utcnow().isoformat(" ", "milliseconds")
            _m: Match = self._match
        _date_time: list[int] = [int(_m.group(index)) for index in self._pattern.match_groups]
        tz_info: tzinfo = datetime.utcnow().tzinfo
        _dt: datetime = datetime(*_date_time, tzinfo=tz_info)
        self._date = _dt.date()
        self._time = _dt.time()
        self._timezone = _dt.astimezone().tzinfo

    def date_time_formatting(self) -> str:
        return datetime.combine(self._date, self._time, self._timezone).isoformat("T", "milliseconds")


if __name__ == '__main__':
    dt = "2023-07-23 14:20:01.620681"
    # logger.info(dt)
    d_t = DateTime(dt)
    logger.info(d_t.date_time_formatting())
