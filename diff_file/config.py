from __future__ import annotations

import json
from datetime import datetime
from pathlib import Path
from typing import Any, Iterator

from loguru import logger

from constants import JsonFileUser, JsonLoginProject, StrNone, StrPath
from custom_exceptions import FileJsonKeyError, FileJsonKeyTypeError, JsonContentKeyError, \
    JsonContentTimestampValueError
from date_time import DateTime


class FileJson:
    """Gets JSON file content.

    Attributes:
        _path (Path): the path to the file;
        _content (dict[str, Any]): the JSON converted content;
    """
    def __init__(self, path: StrPath):
        extension: str = ".json"
        self._path: Path = Path(path).resolve()
        self._extension: str = extension
        self._content: dict[str, Any] = dict()
        self._validate()
        self.read()

    def __str__(self):
        return f"{self._content}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Path {self._path} does not exist")
            raise FileNotFoundError
        if not self._path.is_file():
            logger.error(f"Path {self._path} does not point to the file")
            raise IsADirectoryError
        return

    def read(self):
        with open(self._path, "r+") as f:
            _: dict[str, Any] = json.loads(f.read())
        self._content = _

    def save(self):
        with open(self._path, "w+") as f:
            _: str = json.dumps(self._content)
            f.write(_)

    def __getitem__(self, item):
        if not isinstance(item, str):
            logger.error(f"Ключ {item} должен быть str, но получен {type(item)}")
            raise FileJsonKeyTypeError
        if item not in self._content:
            logger.error(f"Ключ {item} не найден")
            raise FileJsonKeyError
        return self._content.get(item)

    def json_content(self) -> JsonContent:
        return JsonContent(self._path)

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self) != 0

    def __bytes__(self):
        return "\n".join(self._content).encode("utf8")


class JsonContent(FileJson):
    def __init__(self, path: StrPath):
        super().__init__(path)
        self._timestamp: StrNone = self._content.get("timestamp")
        self._files: list[dict[str, list[dict[str, str]]]] = []
        if self._timestamp is None:
            self._date_time: datetime = datetime.utcnow()
        else:
            self._date_time: datetime = datetime.fromisoformat(self._timestamp)

    def __bool__(self):
        return self._timestamp is not None

    def set_files(self):
        self._files = self._content.get("files")

    def __iter__(self) -> Iterator[dict[str, list[dict[str, str]]]]:
        return iter([self[index] for index in range(len(self))])

    def __str__(self):
        return f"{self._timestamp},\n{self._files}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._timestamp},\n{self._files})>"

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._files[item]
        return NotImplemented

    def __len__(self):
        return len(self._files)

    @property
    def logins(self) -> list[str]:
        return [_ for file in iter(self) for _ in file.keys()]

    def iter_logins(self) -> Iterator[str]:
        return iter(self.logins)

    @property
    def json_login_projects(self) -> list[JsonLoginProject]:
        return [
            JsonLoginProject(key, self._files[index].get(key))
            for index, key in enumerate(self.iter_logins())]

    @property
    def json_file_users(self) -> set[JsonFileUser]:
        _: set[JsonFileUser] = set()
        for json_login_project in self.json_login_projects:
            _.update(json_login_project.json_file_users)
        return _

    def get_key(self, item):
        _: int = len(self.logins)
        if not isinstance(item, int) or item not in range(_):
            logger.error(f"Ключ {item} должен быть int и не превышать {_}, но получен {type(item)}")
            raise JsonContentKeyError
        return self.logins[item]

    def iter_json_login_projects(self) -> Iterator[JsonLoginProject]:
        return iter(
            [JsonLoginProject(key, self[index].get(key))
             for index, key in enumerate(self.iter_logins())])

    @property
    def timestamp(self):
        return self._timestamp

    def update_timestamp(self):
        self._content["timestamp"] = self._date_time.isoformat(sep="T", timespec="seconds")
        self.save()
